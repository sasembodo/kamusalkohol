/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import AboutMeScreen from './pages/aboutMe';
import DetailScreen from './pages/detail';
import HomeScreen from './pages/home';
import LoginScreen from './pages/login';
import SplashScreen from './pages/splash';


const Stack = createStackNavigator(); 
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName='Splash Screen'
      >
          <Stack.Screen name='Splash Screen' component={SplashScreen}/>
          <Stack.Screen name='Login Screen' component={LoginScreen}/>
          <Stack.Screen name='Home Screen' component={DrawerHomeNavigation}/>
          <Stack.Screen name='Detail Screen' component={DetailScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const DrawerHomeNavigation=()=>{
  return(
    <Drawer.Navigator
      drawerStyle={{backgroundColor:'#21839a'}}
      drawerContentOptions={{
        activeTintColor: '#21839a',
        activeBackgroundColor: '#f4efe5',
        inactiveTintColor: '#f4efe5',
        inactiveBackgroundColor: '#21839a',
        labelStyle:{
          marginLeft:10
        }
      }}
    >
        <Drawer.Screen name='Home' component={StackDetail}/>
        <Drawer.Screen name='About Me' component={AboutMeScreen}/>
    </Drawer.Navigator>
  )
}

const StackDetail=()=>{
  return(
    <Stack.Navigator>
        <Stack.Screen name='Home' component={HomeScreen}/>
        <Stack.Screen name='Detail' component={DetailScreen}/>
    </Stack.Navigator>
  )
}
