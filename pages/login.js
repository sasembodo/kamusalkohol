import * as React from 'react';
import { Text, View, StyleSheet, TextInput, Button, Alert, Dimensions, SafeAreaView } from 'react-native';
import { useForm, Controller } from 'react-hook-form';

export default ({ navigation }) => {
  const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm({
    defaultValues: {
      name: '',
      age: ''
    }
  });
  const onSubmit = data => {
    const int = parseInt(data.age)
    if(int>=21 && data.name!=''){
      navigation.navigate('Home Screen');
    }else if(!data.name){
      alert('You should insert your name!')
    }else{
      alert(`Sorry ${data.name}, you should 21 years old or higher!`)
    }
  };

  const onChange = arg => {
    return {
      value: arg.nativeEvent.text,
    };
  };

  // console.log('errors', errors);

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.label}>Name</Text>
      <Controller
        control={control}
        render={({field: { onChange, onBlur, value }}) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
          />
        )}
        name="name"
        // rules={{ required: true }}
      />
      <Text style={styles.label}>Age</Text>
      <Controller
        control={control}
        render={({field: { onChange, onBlur, value }}) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
          />
        )}
        name="age"
        // rules={{ required: true }}
      />

      <View style={styles.btnContainer}>

        <View style={styles.button}>
          <Button
            style={styles.buttonInner}
            color
            title="Reset"
            onPress={() => {
              reset({
                name: '',
                age: ''
              })
            }}
          />
        </View>

        <View style={styles.button}>
          <Button
            style={styles.buttonInner}
            color
            title="Submit"
            onPress={handleSubmit(onSubmit)}
          />
        </View>

      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  label: {
    color: '#21839a',
    marginTop: 20,
    marginBottom: 5,
    marginLeft: 0,
  },
  button: {
    marginTop: 40,
    color: 'white',
    height: 40,
    backgroundColor: '#21839a',
    borderRadius: 15,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: '0%',
    paddingHorizontal: '10%',
    backgroundColor: '#f4efe5',
  },
  btnContainer: {
    marginTop: 15,
    paddingHorizontal: '25%'
  },
  input: {
    color:'#21839a',
    backgroundColor: '#ffffff',
    borderColor: '#21839a',
    borderWidth: 1,
    height: 40,
    padding: 10,
    borderRadius: 4,
  },
});
