import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, FlatList, Image, SafeAreaView } from 'react-native';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

export default function DetailScreen({ route, navigation }) {

    const [data, setData]=useState([]);
    const [loading, setLoading]=useState(true);
    const { idDrink } = route.params;
    
    useEffect(() => {
        // console.log(route.params.idDrink, "dsajhdkashkfjhsalkfjaldf")
        axios.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idDrink}`)
          .then(function (response) {
            setData(response.data.drinks);
            setLoading(false);
            // console.log(data, "ashkdsabkfjhjaksf")
            // console.log(JSON.stringify(data, null, 2));
          })
          .catch(function (error) {
            console.log(error);
          })
    }, [])

    
    const thumbContent = (word) => {
        const escaped = escape(word)
        const uri = `https://www.thecocktaildb.com/images/ingredients/${escaped}-Medium.png`
        return uri
    }

    return (
        <SafeAreaView style={styles.bg}>
            <OrientationLoadingOverlay
                visible={loading}
                color="white"
                indicatorSize="large"
                messageFontSize={24}
                message="Loading..."
            />
            <FlatList
                data={data}
                renderItem={({item, index}) =>
                    <View style={styles.item}>
                        
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleTxt}>Cocktail</Text>
                        </View>
                        <View style={styles.content}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                uri: item.strDrinkThumb,
                                }}
                            />
                            <Text style={styles.txtMain}>{item.strDrink}</Text>
                        </View>


                        <View style={styles.titleContainer}>
                            <Text style={styles.titleTxt}>Serving</Text>
                        </View>
                        <View style={styles.serving}>
                            <Text style={styles.txtMain}>           Use {item.strGlass}. {item.strInstructions}</Text>
                        </View>
                        
                        
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleTxt}>Ingredients</Text>
                        </View>
                        
                        {item.strIngredient1 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient1)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure1}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient1}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient2 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient2)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure2}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient2}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient3 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient3)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure3}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient3}</Text>
                                </View>
                            </View>
                        }
                        
                        {item.strIngredient4 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient4)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure4}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient4}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient5 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient5)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure5}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient5}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient6 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient6)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure6}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient6}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient7 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient7)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure7}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient7}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient8 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient8)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure8}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient8}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient9 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient9)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure9}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient9}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient10 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient10)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure10}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient10}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient11 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient11)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure11}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient11}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient12 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient12)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure12}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient12}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient13 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient13)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure13}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient13}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient14 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient14)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure14}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient14}</Text>
                                </View>
                            </View>
                        }

                        {item.strIngredient15 && 
                            <View style={styles.ingredientContent}>
                                <View style={styles.imgIngredientContainer}>
                                    <Image
                                        style={styles.imgIngredient}
                                        source={{
                                        uri: thumbContent(item.strIngredient15)
                                    }}
                                    />
                                </View>                            
                                <View style={styles.ingredient}>
                                    <Text style={styles.txtContent}>{item.strMeasure15}</Text>
                                    <Text style={styles.txtContent}>{item.strIngredient15}</Text>
                                </View>
                            </View>
                        }
                        
                    </View>
                }
                keyExtractor={(item, index) => index.toString()}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    bg:{
        flex: 1,
        backgroundColor: '#d6cebe'
    },
    imgIngredient:{
        width: 180,
        height: 180
    },
    imgIngredientContainer:{
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderRadius: 100
    },
    item: {
        flex:1,
        padding: 10,
        fontSize: 18,
        flexDirection: 'column',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleTxt:{
        fontSize: 24,
        color: '#ffffff'
    },
    titleContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:30,
        marginBottom: 20,
        paddingHorizontal: '13%',
        paddingVertical: 2,
        backgroundColor: '#9f414b',
        borderRadius: 10
    },
    content:{
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    serving:{
        flex: 1,
        marginVertical: 5,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    tinyLogo: {
        width: 200,
        height: 200,
        borderRadius: 25
    },
    txt:{
        color: '#ffffff',
        marginTop: 10,
        marginHorizontal:5,
        fontSize: 20,       
    },
    txtContent:{
        color: '#9f414b',
        marginTop: 10,
        marginHorizontal:5,
        fontSize: 16,    
    },
    txtMain:{
        color: '#9f414b',
        marginTop: 10,
        marginHorizontal:5,
        fontSize: 20,       
    },
    ingredient:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ingredientContent:{
        flexDirection: 'column',
        paddingHorizontal: '20%',
        paddingVertical: '3%',
        marginVertical: '6%',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
