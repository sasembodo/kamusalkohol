import React from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native'

export default function AboutMeScreen() {
    
    return (
        <SafeAreaView style={styles.bg}>
            <View style={styles.header}>
                <Text style={styles.txtTentangSaya}>About Me</Text>
            </View>
            <View style={styles.picContainer}>
                <Image
                  style={{height: 150, width: 150}}
                  source={require('../img/user.png')}
                />
            </View>
            <View style={styles.txtNameContainer}>
                <Text style={styles.txtName}>Sri Aryo Sembodo</Text>
            </View>
            <View style={styles.txtJobContainer}>
                <Text style={styles.txtJob}>React Native Developer</Text>
            </View>
            <View style={styles.skillContainer}>
                <View style={styles.txtPortoContainer}>
                    <Text style={styles.txtPorto}>Portfolio</Text>
                </View>
                <View style={styles.portoContent}>
                    <View style={styles.gitlab}>
                        {/* gitlab */}
                        <Image source={require('../img/gitlab.png')}/>
                        <Text>@sasembodo</Text>
                    </View>
                    <View style={styles.github}>
                        {/* github */}
                        <Image source={require('../img/github.png')}/>
                        <Text>@sasembodo</Text>
                    </View>
                </View>
            </View>
            <View style={styles.kontakContainer}>
                <View style={styles.txtContactContainer}>
                    <Text style={styles.txtContact}>Contact</Text>
                </View>
                <View style={styles.contactContent}>
                    <View style={styles.akun}>
                        <Image
                          source={require('../img/fb.png')}
                          style={{height: 40, width: 40}}
                        />
                        <Text style={styles.txtAkun}>Sri Aryo Sembodo</Text>
                    </View>
                    <View style={styles.akun}>
                        <Image
                          source={require('../img/ig.png')}
                          style={{height: 40, width: 40}}
                        />
                        <Text style={styles.txtAkun}>@sasembodo</Text>
                    </View>
                    <View style={styles.akun}>
                        <Image
                          source={require('../img/twitter.png')}
                          style={{height: 40, width: 40}}
                        />
                        <Text style={styles.txtAkun}>@kelamkalam</Text>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    bg:{
        flex: 1,
        backgroundColor:'#9f414b'
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    txtTentangSaya:{
        color: '#f4efe5',
        fontSize: 36,
        fontFamily: 'Roboto'
    },
    picContainer:{
        margin: 10,
        padding: 25,
        alignItems: 'center'
    },
    txtNameContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtName:{
        fontSize: 24,
        color: '#f4efe5'
    },
    txtJobContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtJob:{
        fontSize: 16,
        color: '#6ee4ff'
    },
    skillContainer:{
        margin: 8,
        height: 130,
        borderRadius: 16,
        backgroundColor: '#f4efe5'
    },
    txtPortoContainer:{
        paddingHorizontal: 8,
        paddingVertical: 5,
        borderBottomWidth:1,
        borderBottomColor: '#003366'
    },
    txtPorto:{
        fontSize: 18,
        color: '#003366'
    },
    portoContent:{
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    gitlab:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    github:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    kontakContainer:{
        margin: 8,
        // height: 130,
        borderRadius: 16,
        backgroundColor: '#f4efe5'
    },
    txtContactContainer:{
        paddingHorizontal: 8,
        paddingVertical: 5,
        borderBottomWidth:1,
        borderBottomColor: '#003366'
    },
    txtContact:{
        fontSize: 18,
        color: '#003366'
    },
    akun:{
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        marginLeft: 80
    },
    txtAkun:{
        color: '#003366',
        fontSize: 16,
        marginLeft: 8
    }
})
