import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image, TextInput, SafeAreaView } from 'react-native';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

export default function HomeScreen({navigation}) {
    const [data, setData]=useState([])
    const [loading, setLoading]=useState(true);
    // Make a request for a user with a given ID
    
    useEffect(() => {
      apiCall()
    }, []);

    const inputFilter = (e) => {
      const word = e.nativeEvent.text
      apiCall(word)
    }

    const apiCall = (word='') =>{
      const url = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${word}`
      axios.get(url)
        .then(function (response){
          setData(response.data.drinks)
          setLoading(false)
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    return (
      <SafeAreaView style={styles.container}>
        <TextInput
          style={styles.searchBar}
          placeholder='Search..'
          onChange={inputFilter}
        />
        <FlatList
          style={{flex: 1}}
          data={data}
          renderItem={({item, index}) => 
          <TouchableOpacity
            onPress={()=>navigation.navigate('Detail Screen', {
              idDrink : item.idDrink
            })}
          >
            <View style={styles.contentContainer}>
              <View style={styles.imgContainer}>
                <Image
                  style={styles.tinyLogo}
                  source={{
                    uri: item.strDrinkThumb,
                  }}
                />
              </View>
              <View style={styles.description}>
                <Text
                  style={{
                    color: '#9f414b',
                    fontSize: 16,
                    fontWeight: 'bold'
                }}>
                    {item.strDrink}
                </Text>
                <Text style={styles.txt}>{item.strAlcoholic}</Text>
                <Text style={styles.txt}>{item.strGlass}</Text>
                <View style={styles.category}>
                  <Text style={styles.txtCategory}>{item.strCategory}</Text>
                </View>
                <Text style={styles.txt}>
                  Ingredients : {item.strIngredient1}   {item.strIngredient2}  {item.strIngredient3}   {item.strIngredient4}   {item.strIngredient5}   {item.strIngredient6}   {item.strIngredient7}   {item.strIngredient8}   {item.strIngredient9}   {item.strIngredient10}   {item.strIngredient11}   {item.strIngredient12}   {item.strIngredient13}   {item.strIngredient14}   {item.strIngredient15}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          }
          keyExtractor={(item, index) => index.toString()}
        />
        <OrientationLoadingOverlay
          visible={loading}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Loading..."
        />
      </SafeAreaView>
    );
  }

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22,
   backgroundColor: '#9f414b'
  },
  contentContainer:{
    flex:1,
    backgroundColor: '#d5cebe',
    flexDirection: 'row',
    paddingHorizontal: '1.5%',
    paddingVertical: '1.5%',
    marginHorizontal: '3%',
    marginVertical: '1%',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },
  imgContainer:{
    flex:1,
    justifyContent: 'flex-start'
  },
  description:{
    flex:2,

  },
  searchBar:{
    backgroundColor: '#ffffff',
    color: '#9f414b',
    height: 40,
    marginHorizontal: '20%',
    marginTop: 10,
    marginBottom: 20,
    borderRadius: 10,
    paddingHorizontal: 15
  },
  tinyLogo: {
    width: 100,
    height: 100,
    borderRadius: 25
  },
  txt: {
    color: '#9f414b',
    fontSize: 12,
    marginTop: 1.5
  },
  category:{
    width:'100%',
    alignItems:'flex-start',
    justifyContent: 'center',
  },
  txtCategory:{
    backgroundColor:'#21839a',
    color:'#f4efe5',
    paddingHorizontal:5,
    paddingVertical:1.5,
    borderRadius: 10,
    fontSize: 12,
    marginTop: 1.5
  }
});
