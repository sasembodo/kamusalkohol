import React, {useEffect} from 'react'
import { StyleSheet, Text, SafeAreaView, View, Image } from 'react-native'
import { color } from 'react-native-reanimated'

export default function SplashScreen({navigation}) {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace('Login Screen')
        }, 3000)
    })
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.logoContainer}>
                <Image
                  style={{height:200, width: 200}}
                  source={require('../img/logo.png')}
                />
            </View>
            <View style={styles.textContainer}>            
                <Text style={{
                  fontSize: 26,
                  fontWeight: 'bold',
                  color: '#21839a'
                }}>
                    KAMUS ALKOHOL
                </Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:'#f4efe5',
        alignItems:'center',
        justifyContent:'center'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    }
})